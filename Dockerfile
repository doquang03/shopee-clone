FROM node:18-alpine

RUN mkdir -p /app
WORKDIR /app
COPY . .
RUN yarn
RUN yarn build
EXPOSE 4173
CMD ["yarn", "preview"]
